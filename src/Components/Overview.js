import React, { useRef, useState } from "react";
import {
    Grid,
    Container,
    Box,
    Typography,
    Button,
    Dialog
} from "@material-ui/core";
import useOnScreen from '../OnScreenHook';
import MenuBookIcon from '@material-ui/icons/MenuBook';
import Form from './Form';

const Overview = ({ config, section }) => {

    const { title, youtube, mainsection, subsection } = config;
    const [dialog, toggleDialog] = useState(false);
    const ref = useRef();
    useOnScreen(ref, '0px');

    return <Container maxWidth="lg" style={{ marginTop: "24px", marginBottom: "24px" }} component="section">
        <span id={section.toLowerCase()} className="anchor" ref={ref} />
        <Box
            fontSize="h2.fontSize"
            color="primary.main"
            textAlign="center"
            mb={3}
            max="auto"
            borderBottom={1}
            fontWeight="fontWeightRegular"
        >
            OVERVIEW
        </Box>
        <Grid container spacing={3}>
            <Grid
                item
                xs={12}
                mx="auto"
            >
                <Box>
                    <Typography variant="h5">{title}</Typography>
                    <iframe
                        src={youtube}
                        allowFullScreen=""
                        width="50%"
                        height="280"
                        frameBorder="0"
                        title="video"
                        style={{ float: "right", minWidth: "315px" }}
                    />
                    {(mainsection !== undefined && section.length > 0) &&
                        mainsection.map((x, i) => <Typography key={i} paragraph>{x}</Typography>)
                    }
                </Box>
            </Grid>
            {subsection !== undefined &&
                <Grid
                    item
                    xs={12}
                >
                    <Typography variant="h5">{subsection.title}</Typography>
                    {(subsection.section !== undefined && subsection.section.length > 0) &&
                        subsection.section.map((x, i) => <Typography key={i} paragraph>{x}</Typography>)
                    }
                    {subsection.button !== undefined &&
                        <Button
                            variant="contained"
                            color="primary"
                            startIcon={<MenuBookIcon />}
                            style={{ display: "flex", margin: "10px auto" }}
                            onClick={() => toggleDialog(!dialog)}
                        >
                            {subsection.button}
                        </Button>
                    }
                </Grid>
            }
        </Grid>
        {/* DIALOG */}
        <Dialog
            open={dialog}
            onClose={() => toggleDialog(!dialog)}
        >
            <Form
                onFormSubmit={data => { toggleDialog(!dialog); console.log(data); }}
                inputs={[
                    { name: "Name", type: "text" },
                    { name: "Email", type: "email" },
                ]}
            >
                <Typography variant="h3" color="primary" gutterBottom align="center">
                    REQUEST BOOKLET
                </Typography>
            </Form>
        </Dialog>
    </Container >
}

export default Overview;