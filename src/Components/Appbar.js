import React, { cloneElement, useState } from 'react';
import PropTypes from 'prop-types';
import {
  AppBar,
  Toolbar,
  CssBaseline,
  useScrollTrigger,
  ButtonGroup,
  Button,
  IconButton,
  Slide,
  SwipeableDrawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Dialog,
  Typography,
} from '@material-ui/core';
import {
  AttachMoney as MoneyIcon,
  Assignment as AssignmentIcon,
  Deck as DeckIcon,
  DeveloperBoard as BoardIcon,
  Photo as PhotoIcon,
  Commute as CommuteIcon,
  Mail as MailIcon,
  Menu as MenuIcon,
  Visibility as VisibilityIcon,
} from '@material-ui/icons';
import Form from './Form';
import './Appbar.css';

const Scroll = props => {
  const { children } = props;

  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
  });

  return cloneElement(children, {
    elevation: trigger ? 4 : 0,
  });
}

Scroll.propTypes = {
  children: PropTypes.element.isRequired,
};

const SelectIcon = ({ value }) => {
  switch (value.toLowerCase()) {
    case "overview":
      return <AssignmentIcon />
    case "prices":
      return <MoneyIcon />
    case "amenities":
      return <DeckIcon />
    case "plans":
      return <BoardIcon />
    case "gallery":
      return <PhotoIcon />
    case "connectivity":
      return <CommuteIcon />
    case "preview":
      return <VisibilityIcon />
    case "contact":
      return <MailIcon />
    default:
      return <AssignmentIcon />
  }
}

const Appbar = props => {

  const { page, sections } = props;

  const [drawer, toggleDrawer] = useState(false);
  const [dialog, toggleDialog] = useState(false);

  return <>
    <CssBaseline />
    <Scroll {...props}>
      <AppBar color="default">
        <Toolbar id="toolbar">
          <a href="./#">
            <img id="logo" src={`${process.env.PUBLIC_URL}/${page.name}/logo.png`} alt="logo" />
          </a>
          <ButtonGroup variant="text" color="primary" aria-label="menu" id="buttongroup">
            {sections.map(x => <Button onClick={() => window.location.href = `#${x.toLowerCase()}`} key={x}>{x}</Button>)}
            {page.phone !== undefined && <Button onClick={() => window.open(`tel:${page.phone}`)}>{page.phone}</Button>}
            <Slide
              direction="left"
              in={useScrollTrigger({ disableHysteresis: true, threshold: document.documentElement.clientHeight / 1.5 })}
              mountOnEnter
              unmountOnExit
              variant="contained"
            >
              <Button
                variant="contained"
                onClick={() => toggleDialog(!dialog)}
              >
                Schedule Site Visit
              </Button>
            </Slide>
          </ButtonGroup>
          <IconButton
            aria-label="delete"
            size="medium"
            id="menudrawertrigger"
            onClick={() => toggleDrawer(true)}
          >
            <MenuIcon fontSize="large" />
          </IconButton>
        </Toolbar>
      </AppBar>
    </Scroll>

    {/* DRAWER */}
    <SwipeableDrawer
      anchor="left"
      open={drawer}
      onClose={() => toggleDrawer(false)}
      onOpen={() => toggleDrawer(true)}
      disableRestoreFocus
    >
      <List>
        {sections.map(x =>
          <ListItem
            button
            key={x}
            onClick={() => {
              window.location.href = `#${x.toLowerCase()}`;
              toggleDrawer(!drawer);
            }}
          >
            <ListItemIcon>{<SelectIcon value={x} />}</ListItemIcon>
            <ListItemText primary={x.toUpperCase()} />
          </ListItem>
        )}
      </List>
      <Button
        variant="contained"
        color="primary"
        style={{ borderRadius: "0px" }}
        onClick={() => { toggleDrawer(!drawer); toggleDialog(!dialog); }}
      >
        Schedule Site Visit
      </Button>
    </SwipeableDrawer>
    <Toolbar id="back-to-top-anchor" />

    {/* DIALOG */}
    <Dialog
      open={dialog}
      onClose={() => toggleDialog(!dialog)}
    >
      <Form onFormSubmit={data => {toggleDialog(!dialog); console.log(data); }} buttonText="SCHEDULE A SITE VISIT WITH ME">
        <Typography variant="h3" color="primary" gutterBottom align="center">
          CONTACT ME
        </Typography>
      </Form>
    </Dialog>
  </>;
}

export default Appbar;