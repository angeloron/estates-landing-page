import React from "react";
import {
    ButtonGroup,
    Button,
    Slide,
    useScrollTrigger,
} from "@material-ui/core";
import {
    Facebook,
    Instagram,
    Twitter,
    Pinterest,
    Reddit,
    LinkedIn,
    YouTube,
    WhatsApp,
    Whatshot,
    Telegram,
    GitHub,
    MenuBook
} from "@material-ui/icons";
import "./Sharing.css";

export const SelectIcon = ({ value }) => {
    switch (value.toLowerCase()) {
        case "facebook": return <Facebook />
        case "instagram": return <Instagram />
        case "twitter": return <Twitter />
        case "pinterest": return <Pinterest />
        case "reddit": return <Reddit />
        case "linkedin": return <LinkedIn />
        case "youtube": return <YouTube />
        case "whatsapp": return <WhatsApp />
        case "whatshot": return <Whatshot />
        case "telegram": return <Telegram />
        case "github": return <GitHub />
        default: return <></>
    }
}

const Sharing = ({ config }) => {

    const { brochure, social } = config;

    return <Slide
        direction="left"
        in={!useScrollTrigger({
            disableHysteresis: true,
            threshold: window.document.body.scrollHeight - window.innerHeight,
        })}
        mountOnEnter
        unmountOnExit
    >
        <ButtonGroup
            orientation="vertical"
            color="primary"
            aria-label="social"
            id="sharingButtonGroup"
        >
            {brochure !== undefined
                &&
                <Button className="sharingButton" onClick={() => window.open(brochure)}>
                    <span id="brochureCaption">BROCHURE</span>
                    <MenuBook />
                </Button>
            }
            {(social !== undefined && social.length > 0)
                &&
                social.map((x, i) =>
                    <Button key={i} onClick={() => window.open(x.link)}>
                        <SelectIcon value={x.name} />
                    </Button>
                )}
        </ButtonGroup>
    </Slide>
}

export default Sharing;