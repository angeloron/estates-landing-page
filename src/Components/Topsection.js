import React from "react";
import { Box, Typography, Divider, Grid } from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import Form from './Form';
import './Topsection.css';

const Topsection = ({ page, config }) => {

    const theme = useTheme();

    const { title, subtitle, formheader } = config;

    return <>
        <Box component="section" minHeight="100vh" id="topsectionbackground">
            <Box component="section" minHeight="100vh" id="topsection" >
                <Grid container
                    spacing={5}
                    direction="row"
                    justify="center"
                    alignItems="center"
                    style={{ minHeight: "100vh" }}
                >
                    <Grid
                        item
                        xs={12}
                        sm={6}
                        mx="auto"
                    >
                        <Typography variant="h4" component="h1">{title}</Typography>
                        <Divider style={{ background: theme.palette.primary.main }} />
                        <Typography variant="subtitle1">{subtitle}</Typography>
                    </Grid>
                    <Grid
                        container
                        item
                        xs={12}
                        sm={4}
                        justify="center"
                        id="topsectionpaper"
                    >
                        <Form onFormSubmit={data => { console.log(data); }} buttonText="SCHEDULE A SITE VISIT WITH ME">
                            {(formheader !== undefined && Object.keys("formheader").length > 0) &&
                                <>
                                    {formheader.title !== undefined &&
                                        <Box>
                                            <Typography variant="body1">{formheader.title}</Typography>
                                        </Box>
                                    }
                                    {(formheader.offers !== undefined && formheader.offers.length > 0) &&
                                        <Box mb={1}>
                                            <Grid
                                                container
                                                justify="space-around"
                                            >
                                                <Grid>
                                                    <pre style={{ margin: 0 }}>
                                                        <Typography variant="caption">{formheader.offers[0]}</Typography>
                                                    </pre>
                                                </Grid>
                                                <Divider orientation="vertical" flexItem />
                                                <Grid>
                                                    <pre style={{ margin: 0 }}>
                                                        <Typography variant="caption">{formheader.offers[1]}</Typography>
                                                    </pre>
                                                </Grid>
                                            </Grid>
                                        </Box>
                                    }
                                </>
                            }
                        </Form>
                    </Grid>
                </Grid>
            </Box>
        </Box>
        <style>
            {`
                #topsectionbackground {
                    background: url("${process.env.PUBLIC_URL}/${page.name}/background.jpg") no-repeat black fixed 50% 50% /cover;
                }
                
            `}
        </style>
    </>
}

export default Topsection;