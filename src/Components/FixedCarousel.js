import React, { useState, useRef } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { Typography, ButtonBase, Fab, Dialog } from '@material-ui/core';
import { Close } from '@material-ui/icons';
import Form from './Form';
import { AutoRotatingCarousel, Slide } from 'material-auto-rotating-carousel';
import useOnScreen from '../OnScreenHook';

const FixedCarousel = ({ page, slides, section, mediaQuery }) => {

    const ref = useRef();
    useOnScreen(ref, '0px');

    const theme = useTheme();
    const classes = useStyles();
    const [carousel, toggleCarousel] = useState(false);
    const [dialog, toggleDialog] = useState(false);

    return <section className={classes.root}>
        <span id={section.toLowerCase()} className="anchor" ref={ref} />
        <ButtonBase
            focusRipple
            className={classes.image}
            focusVisibleClassName={classes.focusVisible}
            style={{
                width: '100%',
            }}
            onClick={() => toggleCarousel(!carousel)}
        >
            <span
                className={classes.imageSrc}
                style={{
                    backgroundImage: `url(${process.env.PUBLIC_URL}/${page.name}/${section.toLowerCase()}.jpg)`,
                    backgroundAttachment: 'fixed',
                    backgroundRepeat: "no-repeat",
                    backgroundSize: "cover",
                }}
            />
            <span className={classes.imageBackdrop} />
            <span className={classes.imageButton}>
                <Typography
                    component="span"
                    variant="h5"
                    color="inherit"
                    className={classes.imageTitle}
                >
                    {section.toUpperCase()}
                    <span className={classes.imageMarked} />
                </Typography>
            </span>
        </ButtonBase>
        <AutoRotatingCarousel
            label='SCHEDULE A VISIT'
            open={carousel}
            onClose={() => toggleCarousel(false)}
            onStart={() => { toggleCarousel(false); toggleDialog(true); }}
            autoplay={false}
            mobile={!mediaQuery}
            style={{ position: 'absolute' }}
        >
            {slides.map((x, i) => <Slide
                key={i}
                media={
                    <>
                        <Fab
                            style={{ position: "absolute", top: 10, right: 10 }}
                            onClick={() => toggleCarousel(false)}
                            color="secondary"
                            size="small"
                        >
                            <Close fontSize="small" />
                        </Fab>
                        <img src={`${process.env.PUBLIC_URL}/${page.name}/${section.toLowerCase()}${i + 1}.jpg`} alt="img" style={{ width: "100%", objectFit: "cover" }} />
                    </>
                }
                mediaBackgroundStyle={{ backgroundColor: theme.palette.primary.main }}
                style={{ backgroundColor: theme.palette.primary.main, position: "relative" }}
                title={x.title === undefined ? "" : x.title}
                subtitle={x.subtitle === undefined ? "" : x.subtitle}
            />
            )}
        </AutoRotatingCarousel>
        <Dialog
            open={dialog}
            onClose={() => toggleDialog(!dialog)}
        >
            <Form onFormSubmit={data => { toggleDialog(!dialog); console.log(data); }}>
                <Typography variant="h3" color="primary" gutterBottom align="center">
                    CONTACT ME
                </Typography>
            </Form>
        </Dialog>
    </section>;
}

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        minWidth: 300,
        width: '100%',
    },
    image: {
        position: 'relative',
        height: 400,
        [theme.breakpoints.down('xs')]: {
            width: '100% !important',
            height: 200,
        },
        '&:hover, &$focusVisible': {
            zIndex: 1,
            '& $imageBackdrop': {
                opacity: 0.15,
            },
            '& $imageMarked': {
                opacity: 0,
            },
            '& $imageTitle': {
                border: '4px solid currentColor',
            },
        },
    },
    focusVisible: {},
    imageButton: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        color: theme.palette.common.white,
    },
    imageSrc: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        backgroundSize: 'cover',
        backgroundPosition: 'center 40%',
    },
    imageBackdrop: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        backgroundColor: theme.palette.common.black,
        opacity: 0.4,
        transition: theme.transitions.create('opacity'),
    },
    imageTitle: {
        position: 'relative',
        padding: `${theme.spacing(2)}px ${theme.spacing(4)}px ${theme.spacing(1) + 6}px`,
    },
    imageMarked: {
        height: 3,
        width: 18,
        backgroundColor: theme.palette.common.white,
        position: 'absolute',
        bottom: -2,
        left: 'calc(50% - 9px)',
        transition: theme.transitions.create('opacity'),
    },
}));

export default FixedCarousel;