import React, { useRef } from "react";
import {
    Grid,
    Container,
    Paper,
    Box,
    Table,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
} from "@material-ui/core";
import useOnScreen from '../OnScreenHook';

const Prices = ({ config, section }) => {

    const { subtitle, rows } = config;
    const ref = useRef();
    useOnScreen(ref, '0px');

    return <Container component="section" maxWidth="lg" style={{ marginTop: "24px", marginBottom: "24px" }}>
        <span id={section.toLowerCase()} className="anchor" ref={ref} />
        <Box
            fontSize="h2.fontSize"
            color="primary.main"
            textAlign="center"
            mb={3}
            max="auto"
            borderBottom={1}
            fontWeight="fontWeightRegular"
        >
            PRICES
        </Box>
        <Paper style={{ padding: "16px", overflow: "hidden" }}>
            <Grid container spacing={3}>
                <Grid
                    item
                    xs={12}
                    mx="auto"
                >
                    <Table aria-label="price table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="center">BHK</TableCell>
                                <TableCell align="center">Carpet Area</TableCell>
                                <TableCell align="center">Price</TableCell>
                                <TableCell align="center">Payment &amp; Unit Plan</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.map((row, i) => (
                                <TableRow key={i}>
                                    <TableCell align="center">{row.bhk}</TableCell>
                                    <TableCell align="center">{row.carpet}</TableCell>
                                    <TableCell align="center">{row.price}</TableCell>
                                    <TableCell align="center">{row.payment}</TableCell>
                                </TableRow>
                            ))}
                            {subtitle !== undefined &&
                                <TableRow>
                                    <TableCell colSpan={4} align="center">{subtitle}</TableCell>
                                </TableRow>
                            }
                        </TableBody>
                    </Table>
                </Grid>
            </Grid>
        </Paper>
    </Container>
}

export default Prices;