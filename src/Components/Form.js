import React, { useState } from "react";
import {
    Paper,
    Grid,
    TextField,
    Button,
    Snackbar,
} from "@material-ui/core";
import MuiAlert from '@material-ui/lab/Alert';

const Form = props => {

    const [formData, loadFormData] = useState({});
    const [snackbar, toggleSnackbar] = useState(false);

    const { onFormSubmit, children, buttonText, inputs } = props;

    const onclickHandler = onFormSubmit === undefined ? e => console.log(formData) : () => { onFormSubmit(formData); loadFormData({}); toggleSnackbar(true) };

    return <Paper style={{ padding: 16, minWidth: "310px", maxWidth: "350px" }} color="primary">
        {children}
        <form onSubmit={e => { e.preventDefault(); onclickHandler() }} >
            <Grid container alignItems="flex-start" spacing={2}>
                {(inputs !== undefined && inputs.length > 0)
                    ?
                    inputs.map((x, i) =>
                        <Grid key={i} item xs={12}>
                            <TextField
                                key={i}
                                fullWidth
                                required
                                name={x.name}
                                variant="filled"
                                type={x.type === undefined ? "text" : x.type}
                                label={x.name}
                                value={formData[x.name] === undefined ? "" : formData[x.name]}
                                onChange={e => loadFormData({ ...formData, [e.target.name]: e.target.value })}
                            />
                        </Grid>
                    )
                    :
                    <>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                required
                                name="name"
                                variant="filled"
                                type="text"
                                label="Name"
                                value={formData.name === undefined ? "" : formData.name}
                                onChange={e => loadFormData({ ...formData, [e.target.name]: e.target.value })}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                name="email"
                                fullWidth
                                required
                                variant="filled"
                                type="email"
                                label="Email"
                                value={formData.email === undefined ? "" : formData.email}
                                onChange={e => loadFormData({ ...formData, [e.target.name]: e.target.value })}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                name="mobile"
                                fullWidth
                                required
                                variant="filled"
                                type="phone"
                                label="Mobile"
                                value={formData.mobile === undefined ? "" : formData.mobile}
                                onChange={e => loadFormData({ ...formData, [e.target.name]: e.target.value })}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                name="date"
                                fullWidth
                                required
                                variant="filled"
                                type="date"
                                value={formData.date === undefined ? "" : formData.date}
                                onChange={e => loadFormData({ ...formData, [e.target.name]: e.target.value })}
                            />
                        </Grid>
                    </>
                }
                <Button
                    color="primary"
                    variant="contained"
                    style={{ width: '100%', margin: '6px', padding: '15px' }}
                    type="submit"
                >
                    {buttonText === undefined ? "SUBMIT" : buttonText.toUpperCase()}
                </Button>
            </Grid>
        </form>
        <Snackbar
            autoHideDuration={4000}
            open={snackbar}
            onClose={() => toggleSnackbar(false)}
        >
            <MuiAlert elevation={6} variant="filled" severity="success">
                THANK YOU!
            </MuiAlert>
        </Snackbar>
    </Paper>

}


export default Form;