import React, { useState } from "react";
import { TextField, ButtonGroup, Button, Grid, Box, Link } from "@material-ui/core";
import { MenuBook } from "@material-ui/icons";
import { SelectIcon } from './Sharing';
import "./Footer.css";

const Footer = ({ section, page, config, sharing, mediaQuery }) => {

    const [formData, loadFormData] = useState({});

    const onclickHandler = () => console.log(formData);

    return <>
        <Box
            id="footerbg"
            style={{ background: `url("/${page.name}/footer.jpg") no-repeat 50% 100% /cover` }}
        />
        <span id={section.toLowerCase()} className="anchor" />
        <form onSubmit={e => { e.preventDefault(); onclickHandler() }} >
            <footer>
                <Grid
                    container
                    item
                    xs={!mediaQuery ? 11 : 6}
                    justify="space-around"
                    alignItems="center"
                    direction="column"
                    id="footer"
                >
                    <Box
                        fontSize="h2.fontSize"
                        fontWeight="fontWeightRegular"
                        color="primary.main"
                        textAlign="center"
                        mb={3}
                        max="auto"
                        borderBottom={1}
                    >
                        {config === undefined || config.formTitle === undefined ? "CONTACT ME" : config.formTitle.toUpperCase()}
                    </Box>
                    <TextField
                        name="name"
                        label="Your Name"
                        fullWidth
                        required
                        type="text"
                        InputProps={{
                            style: {
                                color: "#fff"
                            }
                        }}
                        InputLabelProps={{
                            style: { color: "#fff" },
                        }}
                        onChange={e => loadFormData({ ...formData, [e.target.name]: e.target.value })}
                    />
                    <TextField
                        label="Your E-mail"
                        name="email"
                        fullWidth
                        required
                        type="text"
                        color="primary"
                        InputProps={{
                            style: {
                                color: "#fff"
                            }
                        }}
                        InputLabelProps={{
                            style: { color: "#fff" },
                        }}
                        onChange={e => loadFormData({ ...formData, [e.target.name]: e.target.value })}
                    />

                    <TextField
                        label="Your Message"
                        name="message"
                        fullWidth
                        required
                        type="text"
                        color="primary"
                        multiline
                        rowsMax={5}
                        InputProps={{
                            style: {
                                color: "#fff"
                            }
                        }}
                        InputLabelProps={{
                            style: { color: "#fff" },
                        }}
                        onChange={e => loadFormData({ ...formData, [e.target.name]: e.target.value })}
                    />
                    <Button
                        variant="contained"
                        color="primary"
                        style={{ width: "15px auto" }}
                        type="submit"
                    >
                        {config === undefined || config.buttonText === undefined ? "CONTACT ME" : config.buttonText.toUpperCase()}
                    </Button>
                    {sharing !== undefined
                        &&
                        <ButtonGroup
                            color="primary"
                            style={{ justifyContent: "center" }}
                        >
                            {sharing.brochure !== undefined
                                &&
                                <Button className="sharingButton" onClick={() => window.open(sharing.brochure)}>
                                    <MenuBook />
                                </Button>
                            }
                            {sharing.social !== undefined && sharing.social.map((x, i) =>
                                <Button key={i} onClick={() => window.open(x.link)}>
                                    <SelectIcon value={x.name} />
                                </Button>
                            )}
                        </ButtonGroup>
                    }
                </Grid>
                <Grid
                    container
                    item
                    xs={12}
                    justify="center"
                    direction="row"
                    id="copyright"
                >
                    <span>
                        Made with <span role="img" aria-label="heart">❤️</span> in <Link href="//reactjs.org" target="_blank" color="inherit">ReactJS</Link> by <Link href="//angeloron.com" target="_blank" color="inherit">Ange Loron</Link>
                    </span>
                </Grid>
            </footer>
        </form>
    </>
}

export default Footer;