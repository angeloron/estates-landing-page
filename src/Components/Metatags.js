import React from "react";
import MetaTags from 'react-meta-tags';

const Metatags = ({ page }) => {

    const { name, title, url, description, keywords } = page;

    return <MetaTags>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="theme-color" content="#000000" />
        <meta name="description" content={description} />
        <meta itemProp="name" content={name} />
        <meta itemProp="url" content={url} />
        <meta itemProp="keywords" content={keywords} />
        <meta name="keywords" content={keywords} />
        <meta name="twitter:title" content={title.toUpperCase()} />
        <meta name="twitter:url" content={url} />
        <meta name="twitter:image" content={`${process.env.PUBLIC_URL}/${page.name}/twitter.png`} />
        <meta name="twitter:card" content="summary" />
        <meta property="twitter:description" content={description} />
        <meta property="og:site_name" content={title} />
        <meta property="og:title" content={title} />
        <meta property="og:url" content={url} />
        <meta property="og:description" content={description} />
        <meta property="og:image" content={`${process.env.PUBLIC_URL}/${page.name}/opengraph.png`} />
        <meta property="og:type" content="profile" />
        <link rel="canonical" href={url} />
        <link rel="icon" href={`${process.env.PUBLIC_URL}/${page.name}/favicon.ico`} sizes="16x16" type="image/ico"></link>
        <link rel="apple-touch-icon" href={`${process.env.PUBLIC_URL}/${page.name}/ioslogox192.png`} />
        <title>{title}</title>
    </MetaTags>
}

export default Metatags;