import React, { useRef } from 'react';
import { Container, Box } from "@material-ui/core";
import useOnScreen from '../OnScreenHook';
import "./Preview.css";

const Preview = ({ section, config }) => {

    const ref = useRef();
    useOnScreen(ref, '0px');

    return <Container
        component="section"
        style={{ marginTop: "24px", marginBottom: "24px" }}
    >
        <span id={section.toLowerCase()} className="anchor" ref={ref} />
        <Box
            fontSize="h2.fontSize"
            color="primary.main"
            textAlign="center"
            mb={3}
            max="auto"
            borderBottom={1}
            fontWeight="fontWeightRegular"
        >
            {section.toUpperCase()}
        </Box>
        <div id="iframeOverlay"
            onClick={event => {
                event.target.style.pointerEvents = "none";
                event.target.style.background = "transparent";
                event.currentTarget.children[0].innerText = "";
            }}
        >
            <div>DOUBLE CLICK TO ACTIVATE</div>
        </div>
        <iframe
            src={config.url}
            allowFullScreen
            width="100%"
            height="540"
            frameBorder="0"
            title="residence"
            aria-hidden="false"
            tabIndex="0"
            style={{ border: 0 }}
        />
    </Container>;
}

export default Preview;