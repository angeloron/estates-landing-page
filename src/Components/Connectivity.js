import React, { useRef } from 'react';
import { Container, Box, Grid, Typography, Divider } from "@material-ui/core";
import useOnScreen from '../OnScreenHook';
import "./Connectivity.css";

const Connectivity = ({ section, config, mediaQuery }) => {

    const ref = useRef();
    useOnScreen(ref, '0px');

    return <Container
        component="section"
        style={{ marginTop: "24px", marginBottom: "24px" }}
    >
        <span id={section.toLowerCase()} className="anchor" ref={ref} />
        <Box
            fontSize="h2.fontSize"
            color="primary.main"
            textAlign="center"
            mb={3}
            max="auto"
            borderBottom={1}
            fontWeight="fontWeightRegular"
        >
            {section.toUpperCase()}
        </Box>
        {config.map((x, i) => <Grid
            className={!mediaQuery ? "rightWrapper" : i % 2 === 0 ? "rightWrapper" : "leftWrapper"}
            key={i}
            container
            justify="center"
            alignItems="center"
        >
            <Grid
                item
                xs={12}
                sm={6}
                mx="auto"
                className="textContainer"
            >
                <Typography variant="h4" style={{ textTransform: "uppercase" }}>{x.title}</Typography>
                <Divider />
                <Typography paragraph style={{ marginTop: "15px" }}>{x.paragraph}</Typography>
                <div className={!mediaQuery ? "rightArrow" : i % 2 === 0 ? "rightArrow" : "leftArrow"} />
            </Grid>
            <Grid
                container
                item
                xs={12}
                sm={6}
                justify="center"
                style={{ backgroundImage: `url(./lodhamara/${x.name}.jpg)` }}
                className="photoContainer"
            />
        </Grid>
        )}
    </Container>;
}

export default Connectivity;