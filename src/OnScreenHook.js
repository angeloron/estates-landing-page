import { useState, useEffect } from "react";

export default function useOnScreen(ref, rootMargin = '0px', ...props) {

    
    // State and setter for storing whether element is visible
    const [isIntersecting, setIntersecting] = useState(false);
    
    useEffect(() => {
        const refCurrent = ref.current;
        const observer = new IntersectionObserver(
            ([entry]) => {
                // Update our state when observer callback fires
                setIntersecting(entry.isIntersecting);
            },
            {
                rootMargin
            }
        );

        if (refCurrent) {
            observer.observe(refCurrent);
        }

        return () => {
            observer.unobserve(refCurrent);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []); // Empty array ensures that effect is only run on mount and unmount

    //Checks if the element has "anchor" class in which case it adds it to the url and history
    if (isIntersecting && ref.current !== undefined && ref.current.className === "anchor") {
        if (window.history.pushState) {
            var urlHash = "#" + ref.current.id;
            window.history.pushState(null, null, urlHash);
        }
    }

    return isIntersecting;

}