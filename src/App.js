import React/*, { useState, useEffect }*/ from 'react';
import { CircularProgress, Grid } from '@material-ui/core';
import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import Appbar from './Components/Appbar';
import Metatags from './Components/Metatags';
import ScrollTop from './Components/ScrollTop';
import Topsection from './Components/Topsection';
import Overview from './Components/Overview';
import Prices from './Components/Prices';
import FixedCarousel from './Components/FixedCarousel';
import Connectivity from './Components/Connectivity';
import Preview from './Components/Preview';
import Footer from './Components/Footer';
import Sharing from './Components/Sharing';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import "./App.css";
// import OpengraphReactComponent from "opengraph-react";


const App = () => {

  const config = require('./config.json');
  const mediaQuery = useMediaQuery('(min-width:600px)');

  // const [config, setConfig] = useState({});

  // useEffect(() => {
  //   const source = (process.env.NODE_ENV === "development" || window.location.host === "angeloron.gitlab.io") ? "lodhamara" : window.location.host;
  //   fetch(`${source}/config.json`)
  //     .then(response => response.json())
  //     .then(data => setConfig(data))
  // }, [])

  let { page,
    sections,
    topsection,
    overview,
    amenities,
    prices,
    gallery,
    connectivity,
    plans,
    preview,
    contact,
    sharing,
    theme,
  } = config;

  theme = createMuiTheme(theme);
  theme = responsiveFontSizes(theme);


  const isVisible = (el) => {

    var rect = el.getBoundingClientRect();

    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  }

  const Component = ({ component }) => {
    switch (component.toLowerCase()) {
      case "overview":
        return (overview !== undefined && Object.keys(overview).length > 0) ?
          <Overview config={overview} section="overview" />
          :
          <></>
      case "amenities":
        return (amenities !== undefined && amenities.length > 0)
          ?
          <FixedCarousel
            page={page}
            slides={amenities}
            section="amenities"
            mediaQuery={mediaQuery}
            isVisible={isVisible}
          />
          :
          <></>
      case "prices": {
        return (prices !== undefined && Object.keys(prices).length > 0)
          ?
          <Prices config={prices} section="prices" />
          :
          <></>
      }
      case "gallery":
        return (gallery !== undefined && gallery.length > 0)
          ?
          <FixedCarousel
            page={page}
            slides={gallery}
            section="gallery"
            mediaQuery={mediaQuery}
          />
          :
          <></>
      case "connectivity":
        return (connectivity !== undefined && connectivity.length > 0)
          ?
          <Connectivity
            config={connectivity}
            section="connectivity"
            mediaQuery={mediaQuery}
          />
          :
          <></>
      case "plans":
        return (plans !== undefined && plans.length > 0)
          ?
          <FixedCarousel
            page={page}
            slides={plans}
            section="plans"
            mediaQuery={mediaQuery}
          />
          :
          <></>
      case "preview":
        return (preview !== undefined && Object.keys(preview).length > 0)
          ?
          <Preview
            section={"preview"}
            config={preview}
          />
          :
          <></>
      default:
        return <></>
    }
  }

  return Object.keys(config).length === 0
    ?
    <Grid
      container
      justify="center"
      alignItems="center"
      style={{ width: '100vw', height: '100vh' }}
    >
      <CircularProgress />
    </Grid>
    :
    <ThemeProvider theme={theme}>
      <Metatags page={page} />
      <Appbar page={page} sections={sections} />
      {(topsection !== undefined &&
        Object.keys(topsection).length > 0) &&
        <Topsection page={page} config={topsection} />
      }
      <main>
        {sections.map(x => <Component key={x} component={x} />)}
        {window.innerWidth > 600 && <ScrollTop />}
        {(mediaQuery && sharing !== undefined) && <Sharing config={sharing} />}
      </main>
      {(contact !== undefined && Object.keys(preview).length > 0)
        &&
        <Footer
          section={"contact"}
          config={contact}
          page={page}
          sharing={sharing}
          mediaQuery={mediaQuery}
        />}
      {/* <OpengraphReactComponent
        site={'site that you want to load (url encoding is done for you)'}
        appId="81caeda4-41c0-485c-8b9b-ac39bc22166b"
        //   loader={A component to display while results are being fetched}
        size={'small'}
      /> */}
    </ThemeProvider>;
}

export default App;